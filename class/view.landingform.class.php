<?php

class emuV_Landingform extends emuView
{
    public function build()
    {
        $landingPost = $this->vars['post'];

        ?>
        <form class="landing-form" method="post" action="">

            <h4><?php echo ($title = $landingPost->getCustomField('Form Title')) ? $title : 'Your Details' ?></h4>

            <?php echo $this->emuApp->getMessages('landing-page');?>

            <div class="form-fields">
                <div class="form-row clearfix">
                    <input type="text" name="first_name" placeholder="First Name*" id="first_name" value="<?php echo post_val('first_name')?>" />
                    <input type="text" name="last_name" placeholder="Last Name" id="last_name" value="<?php echo post_val('last_name')?>" />
                </div>
                <div class="form-row clearfix">
                    <input type="text" name="email" placeholder="Your Email*" id="email" value="<?php echo post_val('email')?>" />
                </div>
                <div class="form-row clearfix">
                    <input type="text" name="phone_number" placeholder="Phone Number" id="phone_number" value="<?php echo post_val('phone_number')?>" />
                </div>
            </div>

            <div class="form-submit">
                <input type="submit" name="e-button" class="button" value="Submit" />
            </div>

            <?php $this->emuApp->bindProcessor('landing-form')?>
        </form>
        <?php
    }

}

?>