<?php

class emuLandingContact extends emuDbEntity
{
    function init()
    {
        global $emuLanding;

        $this->dbPrefix = $emuLanding->dbPrefix;
        $this->dbTable = 'landing_contacts';
    }
}