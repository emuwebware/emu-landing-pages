<?php

class emuP_LandingForm extends emuProcessor
{
    public $requiredFields = array('full_name', 'email');

    public function process()
    {
        $this->checkRequiredFields();

        if( !$this->hasRequiredFields )
        {
            $this->emuApp->addMessage( 'landing-page', 'Please provide a name and email address.', 'error' );
            $this->error = true;
            return;
        }

        // Check the email is valid
        if( !$this->emuApp->isValidEmail( post_val( 'email' ) ) )
        {
            $this->emuApp->addMessage( 'landing-page', 'Please provide a valid email address', 'error' );
            $this->error = true;
            return;
        }

        $this->processForm();

        if( !$this->error )
        {
            header( 'Location: '.add_query_arg('success', 'yes', $_SERVER['REQUEST_URI']) );
            exit();
        }
    }

    private function processForm()
    {
        $contact = $this->emuApp->getInstance('emuLandingContact');

        $contact->fullName = post_val('full_name');
        $contact->email = post_val('email');
        $contact->phone = post_val('phone_number');
        $contact->landingPageURI = $_SERVER['REQUEST_URI'];
        $contact->message = post_val('message');
        $contact->dateSubmitted = apply_date_format('db');

        $contact->save();

        $this->sendNotificationEmails($contact);

    }

    private function sendNotificationEmails($contact)
    {

        $templateManager = $this->emuApp->getManager('email-template');

        // Get the email template
        $email_template = $templateManager->getTemplate( 'landing-contact.txt' );

        $tags = array(  'form email' => $contact->email,
                        'landing page' => $contact->landingPageURI,
                        'date submitted' => $contact->dateSubmitted,
                        'message' => $contact->message,
                        'full name' => $contact->fullName,
                        'email' => $contact->email,
                        'phone number' => $contact->phone );

        $template = $templateManager->fillTemplate( $email_template, $tags );

        // Extract the email information and remove the header
        $email = $templateManager->extractEmailData( $template );

        // Send the mail (with the document as attachment)
        $this->emuApp->mail( $email->to, $email->from, $email->subject, $email->content, $email->type, $email->cc, $email->bcc );
    }
}

?>