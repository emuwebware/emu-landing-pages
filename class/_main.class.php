<?php

class emuLanding extends emuApp
{
	public function setAppConfig()
	{
		$this->emuAppID = 'emuLanding';
		$this->menuName = 'Emu Landing Pages';
		$this->dbPrefix = 'emu_l_';
		$this->emailTemplatesEnabled = true;
		// $this->forceInstall = true;
	}

	public function init()
	{
        $this->loadManager('landing');

	}
}

?>