<?php

class emuM_Landing extends emuManager
{
    public function init()
    {
        // Views
        $this->emuApp->registerView( 'landing-form' );

        // Processors
        $this->registerProcessorClass( 'landing-form' );

        // Models
        $this->emuApp->registerModel( 'emuLandingPage', 'model.landingpage.class.php', 'emuPost' );
        $this->emuApp->registerModel( 'emuLandingContact', 'model.landingcontact.class.php', 'emuDbEntity' );
    }

    function registerCustomPostTypes()
    {
        $labels = array(
            'name' => 'Landing Pages',
            'singular_name' => 'Landing Page',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Landing Page',
            'edit_item' => 'Edit Landing Page',
            'new_item' => 'New Landing Page',
            'view_item' => 'View Landing Page',
            'search_items' => 'Search Landing Pages',
            'not_found' => 'No landing pages found',
            'not_found_in_trash' => 'No landing pages found in Trash',
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'property'),
            'capability_type' => 'page',
            'hierarchical' => true,
            'menu_position' => null,
            'supports' => array( 'title', 'excerpt', 'editor', 'author', 'custom-fields', 'revisions', 'thumbnail', 'page-attributes' )
        );

        register_post_type( 'landing-page', $args );
    }

    public function install()
    {
        global $wpdb;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $sql = "CREATE TABLE {$this->emuApp->dbPrefix}landing_contacts (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        firstName varchar(300) default NULL,
        lastName varchar(300) default NULL,
        fullName varchar(300) default NULL,
        message text default NULL,
        email varchar(300) default NULL,
        phone varchar(300) default NULL,
        landingPageURI varchar(300) default NULL,
        dateSubmitted datetime default NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

    }
}

?>