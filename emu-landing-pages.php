<?php
/*
Plugin Name: Emu Landing Pages
Plugin URI: http://www.emuwebware.com
Description: Creates landing pages
Version: 0.1
Author: Emu
Author URI: http://www.emuwebware.com
*/

$plugin_init =  'include_once( "class/_main.class.php" ); '.
                'global $emuLanding; '.
                '$emuLanding = new emuLanding("'.__FILE__.'");';

add_action( 'emu_framework_loaded', create_function('', $plugin_init) );


?>
